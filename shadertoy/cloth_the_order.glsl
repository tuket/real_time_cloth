const float PI = 3.14159;

float D_invgauss(vec3 N, vec3 H)
{
	float A = 2.5;
    float a = 0.9;
    float teta_h = acos(dot(N, H));
    float cnorm = 1.0 / (PI*(1.0+A*a*a));
    float Aexp = pow(A, -1.0/(tan(teta_h)*tan(teta_h)*a*a));
    float sin4 = pow(sin(teta_h), 4.0);
    return cnorm * (1.0 + Aexp / sin4);
}

float Lighting(vec3 N, vec3 V, vec3 R, vec3 L, vec3 H, float R0)
{
    float NdotL = clamp(dot(N, L), 0.0, 1.0); // Lambert Diffuse
    float NdotV = clamp(dot(V, N), 0.0, 1.0); // Gradient
    float HdotV = clamp(dot(H, V), 0.0, 1.0);
    float NdotH = clamp(dot(N, H), 0.0, 1.0);
   
    float F_m = R0 + (1.0 - R0) * pow(1.0 - NdotV, 5.0); // Fresnel for mirrors
    float F_r = R0 + (1.0 - R0) * pow(1.0 - HdotV, 5.0); // Fresnel for rough stuff   
   
    float Cdif = 0.2;
   	
    float diffuse = (1.0 - F_r) * Cdif / PI;
    float specular = (F_r * D_invgauss(N, H)) / (4.0 * (NdotL + NdotV - NdotL*NdotV));
   
    return (diffuse + specular) * NdotL;
}
float Reflectance(float n1, float n2)
{
    return abs((n1 - n2) / (n1 + n2)); // To implement
}
// ---------------------------------------------------------------
float Sphere(vec3 ray, vec3 dir, vec3 center, float radius)
{
 vec3 rc = ray - center;
 float c = dot(rc, rc) - (radius * radius);
 float b = dot(dir, rc);
 float d = b * b - c;
 float t = -b - sqrt(abs(d));
 float st = step(0.0, min(t,d));
 return mix(-1.0, t, st);
}
void mainImage(out vec4 fragColor, in vec2 fragCoord)
{
 vec2 uv = (-1.0 + 2.0 * fragCoord.xy / iResolution.xy) * vec2(iResolution.x / iResolution.y, 1.0);
 vec3 ro = vec3(0.0, 0.0, -3.0);
 vec3 rd = normalize(vec3(uv, 1.0));
 vec3 p = vec3(0.0, 0.0, 0.0);
 float t = Sphere(ro, rd, p, 1.0);
    vec3 L = normalize(-vec3(-1.0 + 2.0 * iMouse.xy / iResolution.xy, -0.25)); // [*] cambiar a 1.0 para ver F_r
  
 vec3 N = normalize(p - (ro + rd * t));
 vec3 V = rd;
 vec3 R = reflect(V, N);
    vec3 H = normalize(V + L);
   
    float inside = clamp(t, 0.0, 1.0);
   
    float c = inside * Lighting(N, V, R, L, H, Reflectance(1.0, 5.5));
    fragColor = vec4(c, c, c, 1.0);
} 